part of 'filters.dart';

abstract final interface class FiltersViewModel extends ChangeNotifier
    implements ViewModel {
  FiltersViewModel();

  static const construct = _FiltersViewModel._;

  void updateSize(Size value);
  void updateItem(Item value);

  double get maximumHeight;
  double get requiredHeight;

  Iterable<Item> get sortedItems;
}

/*
Алгоритм работает таким образом:
0. Вычисляются размеры всех элементов и самого контейнера
1. Элементы сортируются по свойству isSelected
2. На каждой итерации проверяется возможность уместить элемент в текущую строку
  2.1. Если да, то количество элементов в строке инкрементируется
  2.2. Если нет, то создается новая строка
3. Когда элементы заканчиваются, отдается последняя строка
4. По ТЗ вычисляется высота, требуемая для отображения строк

Этот подход вряд ли является самым оптимальным, но работает быстро и позволяет
легко расширять функционал, например:
так как всегда известно количество (выделенных) элементов и строк,
а высота вычисляется их основе, можно по условию скрывать и отображать
остальные строки, можно ввести счетчик скрытых элементов для удобства,
можно добавить все нужные обработчики событий
*/

final class _FiltersViewModel extends FiltersViewModel {
  _FiltersViewModel._({
    required this.spacing,
    required Iterable<String> labels,
  }) : assert(labels.isNotEmpty, null) {
    init();
    _table.addAll({for (final label in labels) label: InfoX.empty});
  }

  final Size spacing;

  late Size _size;
  late final SplayTreeMap<String, Info> _table;

  @override
  void init() {
    _size = Size.zero;
    _table = SplayTreeMap<String, Info>(ItemX.compareLabels);
  }

  @override
  void dispose() {
    _table.clear();
    super.dispose();
  }

  @override
  void updateSize(Size value) {
    if (value.width != _size.width) {
      _size = value;
      return notifyListeners();
    }
  }

  @override
  void updateItem(Item value) {
    _table[value.label] = value.asInfo;
    return notifyListeners();
  }

  @override
  double get maximumHeight {
    try {
      return _sortedLines
          .map(LineX.maxHeight)
          .reduce(LineX.combineHeights(spacing.height));
    } catch (_) {
      return 0;
    }
  }

  @override
  double get requiredHeight {
    final lines = _sortedLines;

    if (lines.isEmpty) {
      return 0;
    }

    final selectedLines = lines.takeWhile(LineX.hasSelected);

    if (selectedLines.length <= 2) {
      return lines
          .take(2)
          .map(LineX.maxHeight)
          .reduce(LineX.combineHeights(spacing.height));
    }

    return selectedLines
        .map(LineX.maxHeight)
        .reduce(LineX.combineHeights(spacing.height));
  }

  @override
  Iterable<Item> get sortedItems sync* {
    final entries = _table.entries.toList()
      ..sort((a, b) => b.value.compareTo(a.value));
    for (final entry in entries) {
      yield (
        label: entry.key,
        isSelected: entry.value.isSelected,
        size: entry.value.size,
      );
    }
  }

  Iterable<Line> get _sortedLines sync* {
    final maxWidth = _size.width;
    final items = sortedItems;

    if (_table.values.any((value) => value.size.isEmpty)) {
      return;
    }

    var bufferWidth = maxWidth;
    var line = LineX.empty;

    var index = 0;
    while (index < items.length) {
      final item = items.elementAt(index);
      final itemWidth =
          item.size.width + (line.itemsCount > 0 ? spacing.width : 0);

      if (itemWidth < bufferWidth) {
        bufferWidth -= itemWidth;
        line = line.copyWith(
          itemsCount: line.itemsCount + 1,
          hasSelected: line.hasSelected || item.isSelected,
          maxHeight: max(line.maxHeight, item.size.height),
        );
        index += 1;
      } else {
        bufferWidth = maxWidth;
        yield line;
        line = LineX.empty;
        index += 0;
      }
    }

    yield line;
  }
}
