part of 'filters.dart';

typedef Filters = FiltersView;

final class FiltersView extends StatefulWidget {
  const FiltersView({required this.labels, super.key});

  final Iterable<String> labels;

  @override
  State<FiltersView> createState() => _FiltersViewState();
}

class _FiltersViewState extends State<FiltersView> {
  _FiltersViewState();

  late final FiltersViewModel viewModel;

  static const spacing = Size(5, 5);

  @override
  void initState() {
    viewModel = FiltersViewModel.construct(
      spacing: spacing,
      labels: widget.labels,
    );
    super.initState();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  bool toggler = false;
  void toggle() => setState(() => toggler = !toggler);

  @override
  Widget build(BuildContext context) => GestureDetector(
        // Для демонстрации раскрытия
        onLongPress: toggle,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: ClipRect(
              child: ListenableBuilder(
                listenable: viewModel,
                builder: (_, __) => AnimatedContainer(
                  duration: const Duration(milliseconds: 300),
                  width: double.infinity,
                  height: toggler
                      ? viewModel.maximumHeight
                      : viewModel.requiredHeight,
                  // Измерение контейнера
                  child: Measure(
                    onChange: viewModel.updateSize,
                    child: Wrap(
                      spacing: spacing.width,
                      runSpacing: spacing.height,
                      children: <Widget>[
                        for (final item in viewModel.sortedItems)
                          Filter(
                            // Измерение элементов
                            onResize: (value) => viewModel
                                .updateItem(item.copyWith(size: value)),
                            // Обработка выделения
                            onSelect: (value) => viewModel
                                .updateItem(item.copyWith(isSelected: value)),
                            model: item,
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
}

typedef Filter = FilterView;

final class FilterView extends StatelessWidget {
  const FilterView({
    required this.model,
    required this.onSelect,
    required this.onResize,
    super.key,
  });

  final Item model;
  final ValueChanged<bool> onSelect;
  final ValueChanged<Size> onResize;

  Color get labelColor => model.isSelected ? Colors.white : Colors.black;
  Color get backgroundColor =>
      model.isSelected ? Colors.blueAccent : Colors.grey;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () => onSelect(!model.isSelected),
        child: Measure(
          onChange: onResize,
          child: Chip(
            backgroundColor: backgroundColor,
            label: Text(model.label, style: TextStyle(color: labelColor)),
          ),
        ),
      );
}
