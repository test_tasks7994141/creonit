part of 'filters.dart';

typedef Info = ({bool isSelected, Size size});
typedef Item = ({String label, bool isSelected, Size size});
typedef Line = ({int itemsCount, bool hasSelected, double maxHeight});

extension BoolX on bool {
  int get asInt => this ? 1 : 0;
}

extension InfoX on Info {
  static const empty = (isSelected: false, size: Size.zero);

  int compareTo(Info other) =>
      isSelected.asInt.compareTo(other.isSelected.asInt);

  Info copyWith({bool? isSelected, Size? size}) =>
      (isSelected: isSelected ?? this.isSelected, size: size ?? this.size);
}

extension ItemX on Item {
  static const empty = (label: '', isSelected: false, size: Size.zero);

  static int compareLabels(String lhs, String rhs) {
    final byLength = lhs.length.compareTo(rhs.length);
    return byLength == 0 ? lhs.compareTo(rhs) : byLength;
  }

  Item copyWith({String? label, bool? isSelected, Size? size}) => (
        label: label ?? this.label,
        isSelected: isSelected ?? this.isSelected,
        size: size ?? this.size,
      );

  Info get asInfo => (isSelected: isSelected, size: size);
}

extension LineX on Line {
  static const empty = (itemsCount: 0, hasSelected: false, maxHeight: 0.0);

  static int itemsCount(Line value) => value.itemsCount;
  static bool hasSelected(Line value) => value.hasSelected;
  static double maxHeight(Line value) => value.maxHeight;

  static double Function(double, double) combineHeights(double spacing) =>
      (accumulator, value) => accumulator + spacing + value;

  Line copyWith({int? itemsCount, bool? hasSelected, double? maxHeight}) => (
        itemsCount: itemsCount ?? this.itemsCount,
        hasSelected: hasSelected ?? this.hasSelected,
        maxHeight: maxHeight ?? this.maxHeight,
      );
}
