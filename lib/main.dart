import 'package:flutter/material.dart';

import 'package:creonit/core/random.dart';
import 'package:creonit/filters/filters.dart';

// Алгоритм и его описание находятся в файле filters/view_model.dart

// Количество фильтров
int get N => 10;

void main() => runApp(const Application());

final class Application extends StatelessWidget {
  const Application({super.key});

  @override
  Widget build(BuildContext context) => const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Home(),
      );
}

final class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) => SafeArea(
        child: Scaffold(
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.zero,
                    child: Text(
                      'FILTERS',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Filters(labels: RandomX.secure.labels(N)),
                ],
              ),
            ),
          ),
        ),
      );
}
