import 'package:flutter/widgets.dart';
import 'package:flutter/rendering.dart';

/// Вызывает [onChange] при изменении размеров [child]
final class Measure extends SingleChildRenderObjectWidget {
  const Measure({
    required Widget child,
    required this.onChange,
    super.key,
  }) : super(child: child);

  final void Function(Size) onChange;

  @override
  RenderObject createRenderObject(BuildContext context) =>
      MeasureRenderObject(onChange);

  @override
  void updateRenderObject(
    BuildContext context,
    covariant MeasureRenderObject renderObject,
  ) =>
      renderObject._onChange = onChange;
}

final class MeasureRenderObject extends RenderProxyBox {
  MeasureRenderObject(this._onChange);

  Size? _size;
  void Function(Size) _onChange;

  @override
  void performLayout() {
    super.performLayout();
    final newSize = child!.size;
    if (newSize != _size) {
      _size = newSize;
      WidgetsBinding.instance.addPostFrameCallback((_) => _onChange(newSize));
    }
  }
}
