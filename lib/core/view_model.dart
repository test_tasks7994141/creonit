abstract interface class ViewModel {
  ViewModel();

  void init();
  void dispose();
}
