extension Let<T extends Object?> on T {
  // (x + y + z).let((r) => r * r)
  R let<R>(R Function(T) expression) => expression(this);
}
