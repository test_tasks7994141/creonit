import 'dart:math';

extension RandomX on Random {
  static final secure = Random.secure();

  /// 1. Случайное число от [from] до [to]
  int _integer(int from, int to) => nextInt(to) + from;

  /// 2. Случайная буква от A до Z
  int get _letter {
    const lettersCount = 26;
    final firstLetterCharCode = 'A'.codeUnitAt(0);
    return _integer(firstLetterCharCode, lettersCount);
  }

  /// 3. Строка случайных букв длиной [length]
  String _label(int length) {
    final values = List.generate(length, (_) => _letter);
    return String.fromCharCodes(values);
  }

  /// 4. Набор случайных строк в количестве [count]
  Iterable<String> labels(int count) sync* {
    for (int i = 0; i < count; i++) {
      final length = _integer(3, 15);
      yield _label(length);
    }
  }
}
